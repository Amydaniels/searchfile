﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace searchfile
{
    class Program
    {
        static void Main(string[] args)
        {
            string resumeFile = "C:/Users/Amarachi/Documents/test.txt";

            Console.Write("Enter word to search and press enter button....");
            string search = Console.ReadLine();

            if (File.Exists(resumeFile))//check if file exist
            {
                using (var sr = new StreamReader(resumeFile))//read the file into a variable
                {
                    int count = 0;
                    // loop through the file to check the keyword
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine(); //Read each line 
                        if (String.IsNullOrEmpty(line)) continue;

                        if (line.IndexOf(search, System.StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            Console.WriteLine("Word Found");
                            count = 0;
                            break;
                        }
                        else
                        {
                            count++;
                            
                        }
                    }
                    if(count >0)
                    {
                        Console.WriteLine("Word not Contained");
                    }
                }
                
            }
            Console.ReadLine();
        }
    }
}
